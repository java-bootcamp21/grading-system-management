# Course Register Management
Per te bere run aplikacionin si fillim ju duhet te beni setup databazen lokalisht me credencialet e tuaj lokale

    spring.datasource.url=jdbc:mysql://localhost:3306/school_register
    spring.datasource.username=root 
    spring.datasource.password=root

Pasi te keni bere db setup beni run aplikacionit tek klasa `GradingRegisterManagementApplication`

pas te beni aplikacionin run navigoni nga browser ne url baze http://localhost:8080 

