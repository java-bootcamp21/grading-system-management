package ikub.grading.register.controller.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ikub.grading.register.domain.dto.UserDto;
import ikub.grading.register.service.UserService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@SpringBootTest
public class UserRestControllerTest {

    private MockMvc mockMvc;

    @Autowired
    protected WebApplicationContext wac;

    @MockBean
    private UserService userService;

    private ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(wac)
                .build();
    }

    @Test
    public void test_create_user_ok() throws Exception {
        var userDto = new UserDto();
        userDto.setFirstname("name");
        userDto.setLastname("surname");
        userDto.setEmail("namesurname@yahoo.com");
        userDto.setPassword("1uuuuuuuU#");
        Mockito.doReturn(userDto).when(userService).create(Mockito.any());
        mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON).content(mapper.writeValueAsString(userDto)))
                .andExpect(status().isOk())
                .andExpect(content().json(mapper.writeValueAsString(userDto)));


    }

    @Test
    public void test_deleteUser_ok() throws Exception {
       //Mockito.doNothing().when(userService).deleteById(Mockito.any());
        mockMvc.perform(delete("/api/users/1"))
                .andExpect(status().isOk());

    }
}
