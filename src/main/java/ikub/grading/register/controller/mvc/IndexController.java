package ikub.grading.register.controller.mvc;

import ikub.grading.register.service.CourseService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    private final CourseService courseService;

    public IndexController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping
    public String getIndex1(Authentication auth, Model model){
        model.addAttribute("isAuthenticated",auth!=null);
        model.addAttribute("courses",courseService.getCourses(null));
        return "index";
    }

}
