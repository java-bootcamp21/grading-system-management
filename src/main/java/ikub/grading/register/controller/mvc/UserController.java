package ikub.grading.register.controller.mvc;

import ikub.grading.register.domain.dto.RegisterDetailsForm;
import ikub.grading.register.domain.dto.RegisterForm;
import ikub.grading.register.domain.dto.UserDto;
import ikub.grading.register.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import static ikub.grading.register.configuration.SecurityUtils.*;

@Controller
@RequestMapping("/users")
public class UserController {

    private final UserService userService;
    
	public UserController(UserService userService) {
		super();
		this.userService = userService;
	}

	@GetMapping("/details/{id}")
    public String getUserDetailsView(@PathVariable Integer id, Model model){
		var registerForm = new RegisterDetailsForm();
		registerForm.setId(id);
        model.addAttribute("userDetailsForm", registerForm);
        return "users/register-details-view";
    }

    @PostMapping("/details/{id}")
    public String registerDetails(@PathVariable Integer id,@ModelAttribute("userDetailsForm") @Valid RegisterDetailsForm user,BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "users/register-details-view";
        }
        user.setId(id);
        userService.registerDetails(user);
        return "redirect:".concat(getAfterDetailsCompletedRedirectUrl());
    }

}
