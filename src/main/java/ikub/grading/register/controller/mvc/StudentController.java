package ikub.grading.register.controller.mvc;

import ikub.grading.register.service.CourseService;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/student")
public class StudentController {

    private final CourseService courseService;

    public StudentController(CourseService courseService) {
        this.courseService = courseService;
    }

    @GetMapping
    public String getStudentIndexView(Authentication auth, Model model){
        model.addAttribute("isAuthenticated",auth!=null);
        model.addAttribute("courses",courseService.getCourses(null));

        return "index";
    }
}
