package ikub.grading.register.controller.mvc;

import ikub.grading.register.domain.dto.RegisterForm;
import ikub.grading.register.domain.dto.UserDto;
import ikub.grading.register.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class AuthController {

    private final UserService userService;

    public AuthController(UserService userService) {
		super();
		this.userService = userService;
	}

	@GetMapping("/login")
    public String getLoginForm(){
        return "auth/login-form";
    }

    @GetMapping("/register")
    public String getUserView(Model model){
        model.addAttribute("user",new RegisterForm());
        return "auth/register-view";
    }

    @PostMapping("/register")
    public String register(@ModelAttribute("user") @Valid RegisterForm user, BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            return "auth/register-view";
        }
        var u = userService.register(user);
        return "redirect:/users/details/"+u.getId();
    }
}
