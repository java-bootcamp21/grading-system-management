package ikub.grading.register.controller.rest;

import ikub.grading.register.domain.dto.AssessmentDtoWithFile;
import ikub.grading.register.domain.dto.CourseDto;
import ikub.grading.register.domain.dto.CourseWithFileDto;
import ikub.grading.register.domain.mapper.CourseMapper;
import ikub.grading.register.service.CourseService;
import ikub.grading.register.service.StorageService;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/courses")
public class CourseRestController {

    private final CourseService courseService;
    private final StorageService storageService;

    public CourseRestController(CourseService courseService, StorageService storageService) {
        this.courseService = courseService;
        this.storageService = storageService;
    }

    @PostMapping
    public ResponseEntity<CourseDto> createCourse(@RequestBody CourseDto req){
        return ResponseEntity.ok(courseService.createCourse(req));
    }

    @GetMapping
    public ResponseEntity<List<CourseDto>> getCourses(@RequestParam(required = false) Integer professorId){
        return ResponseEntity.ok(courseService.getCourses(professorId));
    }

    @GetMapping ("/{id}")
    ResponseEntity<CourseDto> getCourseById(@PathVariable Integer id){
        var course= CourseMapper.toDto(courseService.findCourseById(id));
        return ResponseEntity.ok(course);
    }

    @PutMapping("/{id}")
    public ResponseEntity<CourseDto> updateCourse(@PathVariable Integer id, @RequestBody CourseDto req){
        req.setId(id);
        return ResponseEntity.ok(courseService.updateCourse(req));

    }

    @PostMapping("/{id}/students")
    public ResponseEntity<Void> addStudentToCourse(@PathVariable Integer id,@RequestParam Integer studentId){
        courseService.addStudentToCourse(id,studentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}/students")
    public ResponseEntity<Void> removeStudent(@PathVariable Integer id, @RequestParam Integer studentId){
        courseService.removeStudent(id,studentId);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/{id}/assessments")
    public ResponseEntity<CourseDto> addAssessment(@PathVariable Integer id, @ModelAttribute AssessmentDtoWithFile req){
        return ResponseEntity.ok(courseService.addAssessment(id,req));
    }

    @GetMapping("/{id}/assessments")
    public ResponseEntity<Resource> downloadAssessment(@PathVariable Integer id,@RequestParam(required = false) Integer assessmentId
                                                        ,@RequestParam(required = false) String filename){
        Resource resource;
        if(filename!=null){
            resource = storageService.loadAsResource(filename);
        }else{
            resource = courseService.downloadAssessment(id,assessmentId);
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + resource.getFilename() + "\"").body(resource);
    }

}
