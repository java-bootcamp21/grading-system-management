package ikub.grading.register.controller.rest;

import ikub.grading.register.configuration.rest.RestAuthService;
import ikub.grading.register.domain.dto.rest.LoginRequestDto;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Profile("rest")
@RestController
@RequestMapping("/api/auth")
public class RestAuthController {

    private final RestAuthService authService;

    public RestAuthController(RestAuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    public String token(@RequestBody LoginRequestDto req) {
        return "Bearer ".concat(authService.generateToken(req));
    }

}
