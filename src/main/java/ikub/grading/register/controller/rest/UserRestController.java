package ikub.grading.register.controller.rest;

import ikub.grading.register.domain.dto.RegisterDetailsForm;
import ikub.grading.register.domain.dto.UserDto;
import ikub.grading.register.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static ikub.grading.register.domain.mapper.UserMapper.*;


@RestController
@RequestMapping("/api/users")
public class UserRestController {

    private final UserService userService;

    public UserRestController(UserService userService) {
		super();
		this.userService = userService;
	}

	@PostMapping
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto u){
        return ResponseEntity.ok(userService.create(u));
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUser(@PathVariable Integer id,@RequestBody RegisterDetailsForm req){
        req.setId(id);
        return ResponseEntity.ok(userService.registerDetails(req));
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable Integer id){
        return ResponseEntity.ok(toDto(userService.findById(id)));
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> getUsers(){
        return ResponseEntity.ok(userService.findAll());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable Integer id){
        userService.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
