package ikub.grading.register.aspect;

import ikub.grading.register.domain.exception.ResourceNotFoundException;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class LoggingAspect {

    private final Logger logger = LoggerFactory.getLogger(LoggingAspect.class);

    @Before("ikub.grading.register.aspect.JoinPointsConfig.beforePointcut()")
    public void logServiceMethodAccess(JoinPoint joinpoint){
        logger.info("with signature {} is accessed",joinpoint.getSignature());
    }

    @After("execution(* ikub.grading.register.service.*.*(..))")
    public void logAfterServiceMethodAccess(JoinPoint joinpoint){
        logger.info("Service After with signature {} is completed",joinpoint.getSignature());
    }

    @After("execution(* ikub.grading.register.controller.rest.*.*(..))")
    public void logAfterRestMethodAccess(JoinPoint joinpoint){
        logger.info("Controller After with signature {} is completed",joinpoint.getSignature());
    }

    @AfterReturning("execution(* ikub.grading.register.service.*.*(..))")
    public void logAfterReturnServiceMethodAccess(JoinPoint joinpoint){
        logger.info("Service AfterReturn with signature {} is completed",joinpoint.getSignature());
    }

    @AfterReturning("execution(* ikub.grading.register.controller.rest.*.*(..))")
    public void logAfterReturnRestMethodAccess(JoinPoint joinpoint){
        logger.info("Controller AfterReturn with signature {} is completed",joinpoint.getSignature());
    }

    @AfterThrowing(value = "execution(* ikub.grading.register.service.*.*(..))",throwing = "exception")
    public void logAfterThrowingServiceMethodAccess(JoinPoint joinpoint, Throwable exception){
        logger.info("Service After Exception with signature {} is completed",joinpoint.getSignature());
        logger.info("With exception {}",exception.getMessage());
    }

    @AfterThrowing(value = "execution(* ikub.grading.register.controller.rest.*.*(..))",throwing = "exception")
    public void logAfterThrowingRestMethodAccess(JoinPoint joinpoint, Throwable exception){
        logger.info("Controller AfterReturn with signature {} is completed",joinpoint.getSignature());
        logger.info("With exception {}",exception.getMessage());
    }

    @Around("@annotation(ikub.grading.register.aspect.annotation.MeasureTime)")
    public Object logAroundReturnRestMethodAccess(ProceedingJoinPoint joinpoint) throws Throwable {
        Long startTime = System.currentTimeMillis();
        logger.info("Controller startTime {} {} ",startTime,joinpoint.getSignature());
        Object returnValue = joinpoint.proceed();
        Long endTime = System.currentTimeMillis();
        logger.info("Execution time is {} for controller {}",(endTime-startTime),joinpoint.getSignature());
        return returnValue;
    }

    @Before("@annotation(ikub.grading.register.aspect.annotation.CustomSecurityCheck)")
    public void checkSecurityAspect(){
        String email = null;
    }




}
