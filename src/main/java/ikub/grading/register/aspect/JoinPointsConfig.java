package ikub.grading.register.aspect;

import org.aspectj.lang.annotation.Pointcut;

public class JoinPointsConfig {

    @Pointcut("execution(* ikub.grading.register.service.*.*(..)) || execution(* ikub.grading.register.controller.rest.*.*(..))")
    public void beforePointcut(){

    }
}
