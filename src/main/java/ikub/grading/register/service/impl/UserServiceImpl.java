package ikub.grading.register.service.impl;

import ikub.grading.register.domain.dto.RegisterDetailsForm;
import ikub.grading.register.domain.dto.RegisterForm;
import ikub.grading.register.domain.dto.UserDto;
import ikub.grading.register.domain.exception.ResourceNotFoundException;
import ikub.grading.register.domain.mapper.UserMapper;
import ikub.grading.register.entity.User;
import ikub.grading.register.entity.UserRole;
import ikub.grading.register.repository.UserRepository;
import ikub.grading.register.service.UserService;
import jakarta.validation.Valid;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;

import static ikub.grading.register.domain.mapper.UserMapper.*;


@Validated
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    
    

    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
		super();
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
	}

	@Override
    public UserDto register(@Valid RegisterForm form) {
		
        var user= new User();
        user.setEmail(form.getEmail());
        user.setPassword(passwordEncoder.encode(form.getPassword()));
        user.setRole(UserRole.fromValue(form.getRole()));
        return toDto(userRepository.save(user));
    }

    @Override
    public UserDto registerDetails(@Valid RegisterDetailsForm form) {
        var user = findById(form.getId());
        user.setFirstname(form.getFirstname());
        user.setLastname(form.getLastname());
        return toDto(userRepository.save(user));
    }

    @Override
    public UserDto create(@Valid UserDto user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        var result = userRepository.save(toEntity(user));
        return toDto(result);
    }

    @Override
    public UserDto update(Integer id, @Valid UserDto userDto) {
        var user = findById(id);
        var result = userRepository.save(toUpdate(user,userDto));
        return toDto(result);
    }

    @Override
    public User findById(Integer id) {
        return userRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException(String
                        .format("User with id %s do not exist",id)));
    }

    @Override
    public List<UserDto> findAll() {
        return userRepository.findAll().stream().map(UserMapper::toDto).toList();
    }

    @Override
    public void deleteById(Integer id) {
        var toDelete = findById(id);
        toDelete.setDeleted(true);
        userRepository.save(toDelete);
    }
}
