package ikub.grading.register.service.impl;

import ikub.grading.register.aspect.annotation.MeasureTime;
import ikub.grading.register.configuration.SecurityUtils;
import ikub.grading.register.domain.dto.AssessmentDtoWithFile;
import ikub.grading.register.domain.dto.AssessmentItemDto;
import ikub.grading.register.domain.dto.CourseDto;
import ikub.grading.register.domain.exception.ResourceNotFoundException;
import ikub.grading.register.domain.exception.StudentExistException;
import ikub.grading.register.domain.mapper.AssessmentMapper;
import ikub.grading.register.domain.mapper.CourseMapper;
import ikub.grading.register.entity.AssessmentItem;
import ikub.grading.register.entity.Course;
import ikub.grading.register.entity.User;
import ikub.grading.register.entity.WeightEnum;
import ikub.grading.register.repository.CourseRepository;
import ikub.grading.register.service.CourseService;
import ikub.grading.register.service.FileSystemStorageService;
import ikub.grading.register.service.UserService;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CourseServiceImpl implements CourseService {

    private final UserService userService;
    private final CourseRepository courseRepository;
    private final FileSystemStorageService storageService;

    public CourseServiceImpl(UserService userService, CourseRepository courseRepository, FileSystemStorageService storageService) {
        this.userService = userService;
        this.courseRepository = courseRepository;
        this.storageService = storageService;
    }

    @Override
    public Course findCourseById(Integer id) {
        return courseRepository.findById(id)
                .orElseThrow(()-> new ResourceNotFoundException(String.format("course with id %s not found",id)));
    }

    @MeasureTime
    @Override
    public CourseDto createCourse(CourseDto c) {
        var course = CourseMapper.toEntity(c);
        var userId = SecurityUtils.getLoggedUserId();
        var user = userService.findById(userId);
        course.setProfessor(user);
        course = courseRepository.save(course);
        return CourseMapper.toDto(course);
    }

    @Override
    public List<CourseDto> getCourses(Integer professorId) {
        List<Course> courses;
        courses= professorId!=null?courseRepository.findAllByProfessor_Id(professorId):courseRepository.findAll();
        return courses.stream().map(CourseMapper::toDto).toList();
    }

    @Override
    public CourseDto updateCourse(CourseDto courseDto) {
        var course = findCourseById(courseDto.getId());
        course = CourseMapper.toUpdate(course,courseDto);
        return CourseMapper.toDto(courseRepository.save(course));
    }

    @Override
    public CourseDto addStudentToCourse(Integer courseId, Integer studentId) {
        var student = userService.findById(studentId);
        var course = findCourseById(courseId);
        var studentExist = course.getStudents().stream()
                .filter(s-> s.getId()==studentId).count();
        if(studentExist>0){
            throw new StudentExistException(String.format("Student with id %s already exist in course",studentId));
        }else {
            course.getStudents().add(student);
        }

        return CourseMapper.toDto(courseRepository.save(course));
    }

    @Override
    public CourseDto removeStudent(Integer courseId, Integer studentId) {
        var course = findCourseById(courseId);
        List<User> leftStudents = course.getStudents().stream()
                .filter(s -> s.getId()!=studentId).collect(Collectors.toList());
        course.setStudents(leftStudents);
        return CourseMapper.toDto(courseRepository.save(course));
    }

    @Override
    public CourseDto addAssessment(Integer courseId, AssessmentDtoWithFile req) {
        var course = findCourseById(courseId);
        String filename = UUID.randomUUID().toString().concat(".pdf");
        storageService.store(req.getFile(),filename);
        var assessment = new AssessmentItem();
        assessment.setWeight(WeightEnum.fromType(req.getWeight()).getValue());
        assessment.setName(req.getName());
        assessment.setFilename(filename);
        assessment.setCourse(course);
        course.getAssessments().add(assessment);
        return CourseMapper.toDto(courseRepository.save(course));
    }

    @Override
    public AssessmentItemDto findAssessment(Integer courseId, Integer assessmentId) {
        var course = findCourseById(courseId);
        var assessment = course.getAssessments()
                .stream().filter(a -> a.getId()==assessmentId).findFirst()
                .orElseThrow(()-> new ResourceNotFoundException(String
                        .format("Assessment with id %s not found in course with id %s",assessmentId,courseId)));
        return AssessmentMapper.toDto(assessment);
    }

    @Override
    public Resource downloadAssessment(Integer courseId, Integer assessmentId) {
        var course = findCourseById(courseId);
        var assessment = course.getAssessments()
                .stream().filter(a -> a.getId()==assessmentId).findFirst()
                .orElseThrow(()-> new ResourceNotFoundException(String
                        .format("Assessment with id %s not found in course with id %s",assessmentId,courseId)));
        return storageService.loadAsResource(assessment.getFilename());
    }
}
