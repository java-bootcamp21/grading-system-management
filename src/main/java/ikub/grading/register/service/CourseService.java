package ikub.grading.register.service;

import ikub.grading.register.domain.dto.AssessmentDtoWithFile;
import ikub.grading.register.domain.dto.AssessmentItemDto;
import ikub.grading.register.domain.dto.CourseDto;
import ikub.grading.register.entity.Course;
import org.springframework.core.io.Resource;


import java.util.List;

public interface CourseService {

    Course findCourseById(Integer id);
    CourseDto createCourse(CourseDto course);
    List<CourseDto> getCourses(Integer professorId);
    CourseDto updateCourse(CourseDto courseDto);
    CourseDto addStudentToCourse(Integer courseId, Integer studentId);
    CourseDto removeStudent(Integer courseId, Integer studentId);
    CourseDto addAssessment(Integer courseId, AssessmentDtoWithFile req);
    AssessmentItemDto findAssessment(Integer courseId, Integer assessmentId);
    Resource downloadAssessment(Integer courseId, Integer assessmentId);
}
