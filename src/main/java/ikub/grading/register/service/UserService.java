package ikub.grading.register.service;

import ikub.grading.register.domain.dto.RegisterDetailsForm;
import ikub.grading.register.domain.dto.RegisterForm;
import ikub.grading.register.domain.dto.UserDto;
import ikub.grading.register.entity.User;
import jakarta.validation.Valid;

import java.util.List;

public interface UserService {

    UserDto register(@Valid RegisterForm form);
    UserDto registerDetails(@Valid RegisterDetailsForm form);
    UserDto create(@Valid UserDto user);
    UserDto update(Integer id,@Valid UserDto user);
    User findById(Integer id);
    List<UserDto> findAll();
    void deleteById(Integer id);

}
