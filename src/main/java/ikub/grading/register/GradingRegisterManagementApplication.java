package ikub.grading.register;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.stream.Collectors;

@EnableJpaAuditing(auditorAwareRef = "auditorAware")
@SpringBootApplication
public class GradingRegisterManagementApplication {



    public static void main(String[] args) {
        var name = "name";
        var nameArr = name.split("-");
        name = Arrays.asList(nameArr)
                .stream().map(s -> StringUtils.capitalize(s))
                .collect(Collectors.joining("-"));
        System.err.println("Full String "+name);

        SpringApplication.run(GradingRegisterManagementApplication.class, args);
    }

}
