package ikub.grading.register.configuration;

import ikub.grading.register.entity.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.jwt.Jwt;

public class SecurityUtils {

    public static String getAfterDetailsCompletedRedirectUrl(){
        User loggedUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return switch (loggedUser.getRole()){
            case PROFESSOR -> "/professor";
            case STUDENT -> "/student";
        };
    }

    public static Integer getLoggedUserId(){
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if(authentication!=null && authentication.getPrincipal() instanceof User){
            //handle mvc profile
            return getMvcLoggedUser();
        }else if(authentication!=null && authentication.getPrincipal() instanceof Jwt){
            // handle jwt profile
            return getRestLoggedUser();
        }else {
            //handle null authentication
            return null;
        }
    }

    public static Integer getMvcLoggedUser(){
        var authentication = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return authentication.getId();
    }

    public static Integer getRestLoggedUser(){
        var authentication = (Jwt)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return Integer.valueOf(authentication.getClaim("sub"));
    }
}
