package ikub.grading.register.configuration.rest;

import ikub.grading.register.domain.exception.GeneriExceptionResponse;
import ikub.grading.register.domain.exception.ResourceNotFoundException;
import ikub.grading.register.domain.exception.StudentExistException;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Profile("rest")
@RestControllerAdvice
public class GlobalExceptionHandling {

    @ExceptionHandler
    public ResponseEntity<GeneriExceptionResponse> handleGenericException(StudentExistException exp, HttpServletRequest req){
        var response = new GeneriExceptionResponse(HttpStatus.BAD_REQUEST.value(), req.getRequestURI(), exp.getMessage());
        return new ResponseEntity(response,HttpStatus.BAD_REQUEST);

    }

    @ExceptionHandler
    public ResponseEntity<GeneriExceptionResponse> handleGenericException(ResourceNotFoundException exp, HttpServletRequest req){
        var response = new GeneriExceptionResponse(HttpStatus.BAD_REQUEST.value(), req.getRequestURI(), exp.getMessage());
        return new ResponseEntity(response,HttpStatus.BAD_REQUEST);

    }

}
