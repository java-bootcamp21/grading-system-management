package ikub.grading.register.configuration.mvc;

import ikub.grading.register.entity.User;
import ikub.grading.register.entity.UserRole;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class OnLoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

    private static SimpleGrantedAuthority PROFESSOR_ROLE = new SimpleGrantedAuthority(UserRole.PROFESSOR.name());
    private static SimpleGrantedAuthority STUDENT_ROLE = new SimpleGrantedAuthority(UserRole.STUDENT.name());

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
        User userDetails = (User) authentication.getPrincipal();
        String redirectUrl = getRedirectUrl(userDetails,request.getContextPath());
        response.sendRedirect(redirectUrl);

    }

    private String getRedirectUrl(User u,String redirectUrl){
        if(u.getAuthorities().contains(PROFESSOR_ROLE)){
            redirectUrl = (u.getFirstname()!=null && u.getLastname()!=null)?"professor":"users/details/"+u.getId();
        }else if(u.getAuthorities().contains(STUDENT_ROLE)){
            redirectUrl = (u.getFirstname()!=null && u.getLastname()!=null)?"student":"users/details/"+u.getId();
        }
        return  redirectUrl;
    }
}
