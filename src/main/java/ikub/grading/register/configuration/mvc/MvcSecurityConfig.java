package ikub.grading.register.configuration.mvc;

import ikub.grading.register.configuration.SecurityUserDetailsService;
import ikub.grading.register.entity.UserRole;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

import static org.springframework.security.config.Customizer.*;



@Profile("mvc")
@Configuration
public class MvcSecurityConfig {

    @PostConstruct
    public void init(){
        System.err.println("Security Profile Basic");
    }

    private final SecurityUserDetailsService userDetailsService;
    private final OnLoginSuccessHandler onLoginSuccessHandler;
    private final OnLogoutSuccessHandler onLogoutSuccessHandler;

    
    
    public MvcSecurityConfig(SecurityUserDetailsService userDetailsService, OnLoginSuccessHandler onLoginSuccessHandler,
			OnLogoutSuccessHandler onLogoutSuccessHandler) {
		super();
		this.userDetailsService = userDetailsService;
		this.onLoginSuccessHandler = onLoginSuccessHandler;
		this.onLogoutSuccessHandler = onLogoutSuccessHandler;
	}



	@Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        return http
                .csrf(csrf -> csrf.disable())
                .authorizeHttpRequests(auth -> auth
                        .requestMatchers("/").permitAll()
                        .requestMatchers("/register").permitAll()
                        .requestMatchers("/professor/**").hasAuthority(UserRole.PROFESSOR.name())
                        .requestMatchers("/student/**").hasAuthority(UserRole.STUDENT.name())
                        .anyRequest().authenticated()
                )
                .userDetailsService(userDetailsService)
                .httpBasic(withDefaults())
                .formLogin(form-> form
                        .loginPage("/login").permitAll()
                        .successHandler(onLoginSuccessHandler)
                )
                .logout((logout) -> logout.permitAll()
                .logoutSuccessHandler(onLogoutSuccessHandler)
                )
                .build();
    }
}
