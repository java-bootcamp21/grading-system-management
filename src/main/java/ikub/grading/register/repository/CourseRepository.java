package ikub.grading.register.repository;

import ikub.grading.register.entity.Course;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CourseRepository extends JpaRepository<Course,Integer> {

    List<Course> findAllByProfessor_Id(Integer professorId);
}
