package ikub.grading.register.entity;

import jakarta.persistence.*;

import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "courses")
public class Course extends BaseEntity<Integer>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String code;
    private String name;
    private String catalogData;
    @ManyToOne
    @JoinColumn(name = "professor_id",referencedColumnName = "id")
    private User professor;
    @ManyToMany(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinTable(name = "courses_students",joinColumns = {@JoinColumn(name ="course_id")}
    ,inverseJoinColumns = {@JoinColumn(name = "student_id")})
    private List<User> students = new ArrayList<>();
    @OneToMany(mappedBy = "course",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<AssessmentItem> assessments = new ArrayList<>();
	
    public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCatalogData() {
		return catalogData;
	}
	public void setCatalogData(String catalogData) {
		this.catalogData = catalogData;
	}
	public User getProfessor() {
		return professor;
	}
	public void setProfessor(User professor) {
		this.professor = professor;
	}
	public List<User> getStudents() {
		return students;
	}
	public void setStudents(List<User> students) {
		this.students = students;
	}
	public List<AssessmentItem> getAssessments() {
		return assessments;
	}
	public void setAssessments(List<AssessmentItem> assessments) {
		this.assessments = assessments;
	}
	
	
    
    


}
