package ikub.grading.register.entity;

import ikub.grading.register.domain.exception.ResourceNotFoundException;

import java.util.Arrays;

public enum UserRole {

    PROFESSOR("PROFESSOR"),
    STUDENT("STUDENT");

    private String value;

    UserRole(String value){
        this.value = value;
    }

    public static UserRole fromValue(String value){
        return Arrays.asList(UserRole.values()).stream()
                .filter(role -> role.value.equals(value))
                .findFirst()
                .orElseThrow(()-> new ResourceNotFoundException("Role not found"));
    }
}
