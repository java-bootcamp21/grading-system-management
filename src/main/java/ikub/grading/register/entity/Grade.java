package ikub.grading.register.entity;

import jakarta.persistence.*;



@Entity
@Table(name = "grades")
public class Grade extends BaseEntity<Integer>{
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer grade;
    private String notes;
    @ManyToOne
    @JoinColumn(name ="student_id", referencedColumnName = "id")
    private User student;
    @ManyToOne
    @JoinColumn(name = "assessment_id",referencedColumnName = "id")
    private AssessmentItem assessment;
    
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getGrade() {
		return grade;
	}
	public void setGrade(Integer grade) {
		this.grade = grade;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public User getStudent() {
		return student;
	}
	public void setStudent(User student) {
		this.student = student;
	}
	public AssessmentItem getAssessment() {
		return assessment;
	}
	public void setAssessment(AssessmentItem assessment) {
		this.assessment = assessment;
	}
    
    
}
