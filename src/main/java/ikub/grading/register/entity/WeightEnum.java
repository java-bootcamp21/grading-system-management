package ikub.grading.register.entity;

import ikub.grading.register.domain.exception.ResourceNotFoundException;

import java.util.Arrays;

public enum WeightEnum {

    HOMEWORK("homework",0.3),
    PARTICIPATION("participation",0.2),
    FINAL_EXAM("final_exam",0.25),
    FINAL_PROJECT("final_project",0.25);

    private String type;
    private Double value;

    WeightEnum(String type,Double value){
        this.type =type;
        this.value=value;
    }

    public static WeightEnum fromType(String type){
        return Arrays.asList(WeightEnum.values())
                .stream().filter(e->e.getType().equals(type))
                .findFirst()
                .orElseThrow(()-> new ResourceNotFoundException(String.format("Weight %s not found",type)));
    }

    public String getType() {
        return type;
    }

    public Double getValue() {
        return value;
    }
}
