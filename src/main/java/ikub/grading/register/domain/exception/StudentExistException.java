package ikub.grading.register.domain.exception;

public class StudentExistException extends RuntimeException{

    public StudentExistException(String message) {
        super(message);
    }
}
