package ikub.grading.register.domain.exception;

import java.time.LocalDateTime;

public class GeneriExceptionResponse {
	
	private LocalDateTime date = LocalDateTime.now();
	private Integer statusCode;
	private String path;
	private String message;
	
	public GeneriExceptionResponse(Integer statusCode, String path, String message) {
		super();
		this.statusCode = statusCode;
		this.path = path;
		this.message = message;
	}

	public LocalDateTime getDate() {
		return date;
	}

	public void setDate(LocalDateTime date) {
		this.date = date;
	}

	public Integer getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(Integer statusCode) {
		this.statusCode = statusCode;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
