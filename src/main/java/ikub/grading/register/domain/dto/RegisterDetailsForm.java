package ikub.grading.register.domain.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;


public class RegisterDetailsForm {
    private Integer id;
    @NotEmpty
    @Size(max = 30,message = "Maximum size of firstname is 30")
    private String firstname;
    @NotNull
    @Size(max = 30,message = "Maximum size of lastname is 30")
    private String lastname;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
    
    
}
