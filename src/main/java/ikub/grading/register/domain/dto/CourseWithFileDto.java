package ikub.grading.register.domain.dto;

import org.springframework.web.multipart.MultipartFile;

public class CourseWithFileDto {
    private String code;
    private String name;
    private String catalogData;
    private MultipartFile file;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCatalogData() {
        return catalogData;
    }

    public void setCatalogData(String catalogData) {
        this.catalogData = catalogData;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
