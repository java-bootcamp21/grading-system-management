package ikub.grading.register.domain.dto;

import org.springframework.web.multipart.MultipartFile;

public class AssessmentDtoWithFile {

    private String name;
    private String weight;
    private MultipartFile file;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
