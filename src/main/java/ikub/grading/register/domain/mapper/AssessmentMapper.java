package ikub.grading.register.domain.mapper;

import ikub.grading.register.domain.dto.AssessmentItemDto;
import ikub.grading.register.entity.AssessmentItem;
import ikub.grading.register.entity.WeightEnum;

public class AssessmentMapper {

    public static AssessmentItemDto toDto(AssessmentItem item){
        var res = new AssessmentItemDto();
        res.setId(item.getId());
        res.setName(item.getName());
        res.setFilename(item.getFilename());
        res.setWeight(String.valueOf(item.getWeight()));
        res.setCourseId(item.getCourse().getId());
        res.setCourseName(item.getCourse().getName());
        return res;
    }

    public static AssessmentItem toEntity(AssessmentItemDto item){
        var res = new AssessmentItem();
        res.setName(item.getName());
        res.setFilename(item.getFilename());
        res.setWeight(WeightEnum.fromType(item.getWeight()).getValue());
        return res;
    }

}
