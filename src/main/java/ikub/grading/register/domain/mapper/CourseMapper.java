package ikub.grading.register.domain.mapper;

import ikub.grading.register.domain.dto.CourseDto;
import ikub.grading.register.entity.Course;

public class CourseMapper {

    public static CourseDto toDto(Course c){
        var course = new CourseDto();
        course.setId(c.getId());
        course.setName(c.getName());
        course.setCode(c.getCode());
        course.setCatalogData(c.getCatalogData());
        course.setProfessorId(c.getProfessor().getId());
        var professorName = c.getProfessor().getFirstname()
                .concat(" ").concat(c.getProfessor().getLastname());
        var totalAssignments = c.getAssessments().stream().count();
        var totalStudents = c.getStudents().stream().count();
        course.setProfessorName(professorName);
        course.setTotalAssignments(totalAssignments);
        course.setTotalStudents(totalStudents);
        return course;
    }

    public static Course toEntity(CourseDto c){
        var course = new Course();
        course.setName(c.getName());
        course.setCode(c.getCode());
        course.setCatalogData(c.getCatalogData());
        return course;
    }

    public static Course toUpdate(Course c, CourseDto dto){
        c.setCode(dto.getCode());
        c.setName(dto.getName());
        c.setCatalogData(dto.getCatalogData());
        return c;
    }
}
