package ikub.grading.register.domain.mapper;

import ikub.grading.register.domain.dto.UserDto;
import ikub.grading.register.entity.User;
import ikub.grading.register.entity.UserRole;

public class UserMapper {

    public static User toEntity(UserDto u){
    	var user = new User();
    	user.setFirstname(u.getFirstname());
    	user.setLastname(u.getLastname());
    	user.setEmail(u.getEmail());
    	user.setPassword(u.getPassword());
    	user.setRole(UserRole.fromValue(u.getRole()));
        return user;
    }

    public static UserDto toDto(User u){
    	var user = new UserDto();
    	user.setId(u.getId());
    	user.setFirstname(u.getFirstname());
    	user.setLastname(u.getLastname());
    	user.setEmail(u.getEmail());
    	user.setPassword(u.getPassword());
    	user.setRole(u.getRole().name());
        return user;
    }

    public static User toUpdate(User u, UserDto d){
        u.setFirstname(d.getFirstname());
        u.setLastname(d.getLastname());
        return u;
    }


}
